# Bonk Bot
A Discord bot that sends people to horny jail.

_I tested this again on Aug 26, 2024 and it was non-functional/running extremely
slowly. I'm not going to bother fixing it though_

# Setup
## Infrastructure
Sorry, this was all done manually. serverless or Amazon CDK would be nice but was
discovered too late.

![Infrastructure](/infrastructure.png)

The Lambda function hooked into the API Gateway will contain all the files in
src/bonkbot-entry and the other will receive the files in src/bonkbot-runner.

Set the DISCORD_TOKEN environment variable in the bonkbot-runner lambda.


## Dependency layers
[ffmpeg-aws-lambda-layer](https://github.com/serverlesspub/ffmpeg-aws-lambda-layer)


We added a `python` symlink into the venv environment to get the directory
structure for the lambda layer correct. This allows us to do
```
zip -r bonkbot-dependencies.zip python/lib/python3.8/site-packages/*
```
in the top-level folder to create a valid dependency layer for AWS Lambda.


## Configuring Slash commands
`rest.json` defines the slash command interface. Send that as a POST body to
discord to an endpoint like `https://discord.com/api/v8/applications/APP_ID/commands`.


# Usage
Add the bot to your guild using
[this OAUTH link](https://discord.com/api/oauth2/authorize?client_id=791009653605662781&permissions=53478400&scope=bot%20applications.commands)

This makes available a `/bonk` command.
