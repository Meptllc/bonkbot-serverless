import boto3
import json
import discord_interactions as di

def lambda_handler(event, context):
    response = di.preprocess(event)
    if response:
        return response

    data = event.get('body-json')

    # Discord Interactions requires a response within 3 seconds. We'll send a
    # "Deferred response" and fire a lambda to do further work.
    client = boto3.client('lambda')
    client.invoke(FunctionName='bonkbot-runner',
                  InvocationType='Event',
                  Payload=json.dumps(data))
    return di.deferred_message()
