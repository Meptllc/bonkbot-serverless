from bonkbot import bonk

# Manually run the bonk function with fake Discord Interaction data.
bonk({
    "application_id": "791009653605662781",
    "channel_id": "791017575848345612",
    "data": {
        "id": "866384548798136340",
        "name": "bonk",
        "options": [
            {
                "name": "user",
                "type": 6,
                "value": "791009653605662781"
            }
        ],
        "resolved": {
            "members": {
                "791009653605662781": {
                    "avatar": None,
                    "is_pending": False,
                    "joined_at": "2020-12-22T19:03:54.651000+00:00",
                    "nick": None,
                    "pending": False,
                    "permissions": "247081717313",
                    "premium_since": None,
                    "roles": [
                        "791018196953202710"
                    ]
                }
            },
            "users": {
                "791009653605662781": {
                    "avatar": "b91aead99d8389e38b17d4caa00bd126",
                    "bot": True,
                    "discriminator": "5745",
                    "id": "791009653605662781",
                    "public_flags": 0,
                    "username": "BonkBot"
                }
            }
        }
    },
    "guild_id": "791017575307804673",
    "id": "870086883206643772",
    "member": {
        "avatar": None,
        "deaf": False,
        "is_pending": False,
        "joined_at": "2020-12-22T19:01:26.435000+00:00",
        "mute": False,
        "nick": None,
        "pending": False,
        "permissions": "274877906943",
        "premium_since": None,
        "roles": [
            "791022783282675712"
        ],
        "user": {
            "avatar": "331f017f163a542787273e70f99a8367",
            "discriminator": "1925",
            "id": "325085847944691712",
            "public_flags": 0,
            "username": "Meptllc"
        }
    },
    "token": "aW50ZXJhY3Rpb246ODcwMDg2ODgzMjA2NjQzNzcyOml3QmlNN3lSTFhlSmdzRjFrbXFKRUV6VHFRSW9DU1NXV3VIRFRESGdsTTZsWmcxa2xVNnhQS2gwS3F6ZGRmRldNemdib2VFU0o0dkx0SnZvYlJ4TnJWa1JRSHZwejJyNGhXTEcwWlFrOENDTEJiZWE2YlRZekZQSlVZVDFlSkF6",
    "type": 2,
    "version": 1
})

