import bonkbot
import asyncio

def lambda_handler(event, context):
    # Seems like lambda reuses Python sessions.
    asyncio.set_event_loop(asyncio.new_event_loop())

    bonkbot.bonk(event)
    return { 'statusCode': 200 }

