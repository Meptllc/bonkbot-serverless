import asyncio
import os

import aws_integrations as aws
import discord
import requests

DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")
BONKER_ROLE = 'bonk stick'
SOUND_RESOURCE = os.path.dirname(__file__) + '/resources/bonk.opus';

class BonkBotError(Exception):
    pass

def verify_bonker_role(bonker):
    has_role = False
    for role in bonker.roles:
        if role.name == BONKER_ROLE:
            has_role = True

    if not has_role:
        raise BonkBotError(f'You do not have the "{BONKER_ROLE}" role.')


def resolve_target_jail(data):
    try:
        target_jail = data['data']['resolved']['channels'].popitem()[0]
    except KeyError:
        target_jail = aws.get_jail_for_guild(data['guild_id'])

    if not target_jail:
        raise BonkBotError('Horny jail not configured. Please specify the jail option.')
    return int(target_jail)


def verify_target_jail(guild, target_jail):
    channel = guild.get_channel(target_jail)
    if channel.type != discord.ChannelType.voice:
        raise BonkBotError(f'The jail must be a voice channel.')


def verify_targets_in_voice(guild, bonker, bonkee):
    if not bonker.voice or not bonker.voice.channel:
        raise BonkBotError("You aren't connected to a voice channel.")
    if not bonkee.voice or not bonkee.voice.channel:
        raise BonkBotError(f'{bonkee.name} is not connected to a voice channel.')
    if bonker.voice.channel.id != bonkee.voice.channel.id:
        raise BonkBotError(f'{bonkee.name} is not in the same channel as you.')


# Init discord client to run bonk command synchronously.
def bonk(data):
    def send_callback_message(message):
        print(message)
        app_id = data['application_id']
        interaction_token = data['token']
        callback_url = f'https://discord.com/api/v8/webhooks/{app_id}/{interaction_token}'
        requests.post(callback_url, json={ 'content': message })


    intents = discord.Intents.default()
    intents.members = True
    client = discord.Client(intents=intents)

    @client.event
    async def on_ready():
        guild = client.get_guild(int(data['guild_id']))
        bonker = guild.get_member(int(data['member']['user']['id']))
        bonkee = guild.get_member(int(data['data']['resolved']['users'].popitem()[0]))
        try:
            verify_bonker_role(bonker)
            target_jail = resolve_target_jail(data)
            verify_target_jail(guild, target_jail)
            verify_targets_in_voice(guild, bonker, bonkee)
        except BonkBotError as e:
            send_callback_message(str(e))
            await client.close()
            return


        aws.set_jail_for_guild(data['guild_id'], target_jail)
        send_callback_message(f'{bonker.name} bonked {bonkee.name}.')

        channel = guild.get_channel(target_jail)
        voice = await channel.connect()
        await bonkee.move_to(channel)

        voice.play(discord.FFmpegOpusAudio(SOUND_RESOURCE, codec='copy'))

        # Wait for the audio to complete playing.
        await asyncio.sleep(3)
        await client.close()

    client.run(DISCORD_TOKEN)
